package com.esgi.unitest.todolist;

import com.esgi.unitest.todolist.exception.UserException;
import com.esgi.unitest.todolist.components.todolist.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

public class UserTest {


    // SETUP -----------------------------------------------------------------------------------------------------------


    User user;

    @BeforeEach
    public void set_up() {
        this.user = new User(
                "firstname",
                "lastname",
                20,
                "test@email.com",
                "test_password"
        );
    }


    // TESTS -----------------------------------------------------------------------------------------------------------


    @Test
    public void should_return_true_if_user_is_valid() throws UserException {
        Assertions.assertTrue(this.user.is_valid());
    }

    @Test
    public void should_throw_an_error_if_user_not_valid_bcs_age() throws UserException {
        this.user.setAge(1);
        Assertions.assertThrows(UserException.class, () -> this.user.is_valid());
    }

    @Test
    public void should_throw_an_error_if_user_not_valid_bcs_email() throws UserException {
        this.user.setEmail("1");
        Assertions.assertThrows(UserException.class, () -> this.user.is_valid());
    }

    @Test
    public void should_throw_an_error_if_user_not_valid_bcs_password() throws UserException {
        this.user.setPassword("1");
        Assertions.assertThrows(UserException.class, () -> this.user.is_valid());
    }

    @Test
    public void should_throw_an_error_if_user_not_valid_bcs_firstname() throws UserException {
        this.user.setFirstname("1");
        Assertions.assertThrows(UserException.class, () -> this.user.is_valid());
    }

    @Test
    public void should_throw_an_error_if_user_not_valid_bcs_lastname() throws UserException {
        this.user.setLastname("1");
        Assertions.assertThrows(UserException.class, () -> this.user.is_valid());
    }

    @Test
    public void should_return_true_if_email_has_valid_format() throws UserException {
        this.user.setEmail("ahmedine@gmail.com");
        Assertions.assertTrue(this.user.valid_email_format());
    }

    @Test
    public void should_return_an_error_if_email_not_valid() throws Exception {
        this.user.setEmail("0");
        Assertions.assertThrows(UserException.class, () -> this.user.valid_email_format());
    }

    @Test
    public void should_return_true_if_password_is_between_8_and_40_char() throws UserException {
        this.user.setPassword("azertyuiop123");
        Assertions.assertTrue(user.valid_password());
    }

    @Test
    public void should_throw_an_error_if_password_is_to_short() {
        this.user.setPassword("0");
        Assertions.assertThrows(UserException.class, () -> this.user.valid_password());
    }

    @Test
    public void should_throw_an_error_if_password_is_to_long() {
        String word = "";
        for (int i = 0; i < 50; i++) {
            word += i;
        }

        this.user.setPassword(word);
        Assertions.assertThrows(UserException.class, () -> this.user.valid_password());
    }

    @Test
    public void should_return_true_if_firstname_has_valid_format() throws UserException {
        this.user.setFirstname("ahmedine");
        Assertions.assertTrue(this.user.valid_firstname());
    }

    @Test
    public void should_return_an_error_if_firstname_has_not_valid_format() {
        this.user.setFirstname("Diablox9");
        Assertions.assertThrows(UserException.class, () -> this.user.valid_firstname());
    }

    @Test
    public void should_return_true_if_lastname_has_valid_format() throws UserException {
        this.user.setFirstname("benabdelouhab");
        Assertions.assertTrue(this.user.valid_lastname());
    }

    @Test
    public void should_return_an_error_if_lastname_has_not_valid_format() throws UserException {
        this.user.setLastname("zaertyu123ezr");
        Assertions.assertThrows(UserException.class, () -> this.user.valid_lastname());
    }

    @Test
    public void should_return_true_if_his_age_is_13_years_or_more() throws UserException {
        this.user.setAge(22);
        Assertions.assertTrue(user.valid_age());
    }

    @Test
    public void should_throw_an_error_if_user_has_less_than_13_years() {
        this.user.setAge(0);
        Assertions.assertThrows(UserException.class, () -> user.valid_age());
    }

}
