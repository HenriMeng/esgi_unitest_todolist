package com.esgi.unitest.todolist;

import com.esgi.unitest.todolist.exception.TodolistException;
import com.esgi.unitest.todolist.components.todolist.model.Item;
import com.esgi.unitest.todolist.components.todolist.model.Todolist;
import com.esgi.unitest.todolist.components.todolist.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

public class TodolistTest {


    // SETUP -----------------------------------------------------------------------------------------------------------


    Todolist todolist;
    User user;
    Item item;
    LocalDateTime date_now;
    LocalDateTime date_plus_30m;
    LocalDateTime date_minus_30m;


    @BeforeEach
    public void set_up() {
        this.user = new User(
                "firstname",
                "lastname",
                20,
                "test@email.com",
                "test_password"
        );

        this.date_now = LocalDateTime.now();
        System.out.println(date_now);

        this.date_plus_30m = LocalDateTime.now().plusMinutes(30);
        System.out.println(date_plus_30m);

        this.date_minus_30m = LocalDateTime.now().minusMinutes(30);
        System.out.println(date_minus_30m);

        this.item = new Item(
                "test_title",
                "test_content",
                this.date_now
        );

        this.todolist = new Todolist(this.user);
    }


    // TESTS ----------------------------------------------------------------------------------------------------------


    @Test
    public void should_add_item_in_todolist() {
        this.todolist.add_item(this.item);
        Assertions.assertEquals(1, this.todolist.getItems().size());
    }


    @Test
    public void should_throw_an_error_if_title_item_already_exists() {
        this.todolist.setItems(List.of(this.item));
        Assertions.assertThrows(TodolistException.class, () -> this.todolist.valid_items_name(this.item.getTitle()));
    }


    @Test
    public void should_return_true_if_title_item_not_exists() throws TodolistException {
        Assertions.assertTrue(this.todolist.valid_items_name(this.item.getTitle()));
    }


    @Test
    public void verify_if_items_not_null() {
        Assertions.assertNotNull(this.todolist.getItems());
    }


    @Test
    public void should_return_true_if_items_size_equals_alert_email() {
        this.todolist.setAlert_email(0);
        Assertions.assertThrows(TodolistException.class, () -> this.todolist.valid_alert_email());
    }


    @Test
    public void should_return_false_if_items_size_not_equals_alert_email() throws TodolistException{
        Assertions.assertFalse(this.todolist.valid_alert_email());
    }


    @Test
    public void should_return_true_if_size_items_less_than_max_size() throws TodolistException {
        Assertions.assertTrue(this.todolist.valid_items_size());
    }


    @Test
    public void should_return_true_if_size_items_eq_or_more_than_max_size() {
        this.todolist.setMax_size(0);
        Assertions.assertThrows(TodolistException.class,() -> this.todolist.valid_items_size());
    }

}
