package com.esgi.unitest.todolist;

import com.esgi.unitest.todolist.exception.ItemException;
import com.esgi.unitest.todolist.components.todolist.model.Item;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

public class ItemTest {

    // SETUP -----------------------------------------------------------------------------------------------------------

    Item item;
    LocalDateTime date_now;

    @BeforeEach
    public void set_up() {
        this.date_now = LocalDateTime.now();
        System.out.println(date_now);

        this.item = new Item(
                "test_title",
                "test_content",
                this.date_now
        );
    }

    // TESTS -----------------------------------------------------------------------------------------------------------

    @Test
    public void should_return_true_if_valid() throws ItemException {
        Assertions.assertTrue(this.item.is_valid());
    }

    @Test
    public void should_throw_an_error_if_not_valid_bcs_of_content() {
        String word = "";
        for (int i = 0; i < 1500; i++) {
            word += i;
        }
        this.item.setContent(word);
        Assertions.assertThrows(ItemException.class, () -> this.item.is_valid());
    }

    @Test
    public void should_return_true_if_content_is_valid() throws ItemException {
        Assertions.assertTrue(this.item.valid_content());
    }

    @Test
    public void should_throw_an_error_if_content_not_valid() {
        String word = "";
        for (int i = 0; i < 1500; i++) {
            word += i;
        }

        this.item.setContent(word);
        Assertions.assertThrows(ItemException.class, () -> this.item.valid_content());
    }

}
