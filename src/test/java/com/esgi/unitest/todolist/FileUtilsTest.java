package com.esgi.unitest.todolist;

import com.esgi.unitest.todolist.util.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

public class FileUtilsTest {

    // SETUP -----------------------------------------------------------------------------------------------------------

    @BeforeEach
    public void set_up() {

    }

    // TESTS -----------------------------------------------------------------------------------------------------------

    @Test
    public void test_say_hello_should_return_hello() {
        Assertions.assertEquals("Hello !", FileUtils.say_hello());
    }

}
