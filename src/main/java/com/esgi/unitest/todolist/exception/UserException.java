package com.esgi.unitest.todolist.exception;

public class UserException extends Exception {
    public UserException(String errorMessage) {
        super(errorMessage);
    }
}
