package com.esgi.unitest.todolist.exception;

public class ItemException extends Exception {
    public ItemException(String errorMessage) {
        super(errorMessage);
    }
}
