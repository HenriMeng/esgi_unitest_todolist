package com.esgi.unitest.todolist.components.todolist.model;

import com.esgi.unitest.todolist.exception.ItemException;

import java.time.LocalDateTime;

public class Item {

    public String title;
    String content;
    LocalDateTime creation_date;

    public Item(String title, String content, LocalDateTime creation_date) {
        this.title = title;
        this.content = content;
        this.creation_date = creation_date;
    }


    // METHODS --------------------------------------------------------------------------------------------


    public boolean is_valid() throws ItemException {
        if(this.valid_content()){
            return true;
        }
        throw new ItemException("not valid");
    }

    public boolean valid_content() throws ItemException {

        if (this.content.length() <= 1000) {
            return true;
        }

        throw new ItemException("content too long (1000 char max)");
    }

    // GET&SET --------------------------------------------------------------------------------------------


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDateTime creation_date) {
        this.creation_date = creation_date;
    }
}
