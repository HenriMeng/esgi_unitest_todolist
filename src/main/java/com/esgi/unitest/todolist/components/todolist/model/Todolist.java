package com.esgi.unitest.todolist.components.todolist.model;

import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.core.encoder.EchoEncoder;
import com.esgi.unitest.todolist.exception.TodolistException;

import java.util.ArrayList;
import java.util.List;

public class Todolist {

    User       user;
    List<Item> items;
    int        alert_email;
    int max_size;

    public Todolist(User user){
        this.user = user;
        this.items = new ArrayList<>();
        this.alert_email = 8;
        this.max_size = 10;
    }

    public void add_item(Item item) {
        this.items.add(item);
    }

    public boolean valid_items_name(String title) throws TodolistException {
        for (Item item : this.items) {
            if (item.title == title) {
                throw new TodolistException("title already exists");
            }
        }

        return true;
    }

    public boolean valid_alert_email() throws TodolistException {
        if (this.items.size() == this.alert_email) {
            throw new TodolistException("Attention il ne reste plus que " + (max_size - items.size()));
        }
        return false;

    }

    public boolean valid_items_size() throws TodolistException {
        if (this.items.size() == this.max_size) {
            throw new TodolistException("Todolist is full (max items: 10)");
        }

        return true;
    }


    //TODO timer 30 minutes
    //TODO EmailSenderService


    // GET&SET --------------------------------------------------------------------------------------------

    public User getUser() {
        return user;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public int getAlert_email() {
        return alert_email;
    }

    public void setAlert_email(int alert_email) {
        this.alert_email = alert_email;
    }

    public int getMax_size() {
        return max_size;
    }

    public void setMax_size(int max_size) {
        this.max_size = max_size;
    }
}


