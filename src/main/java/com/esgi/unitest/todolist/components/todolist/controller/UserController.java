package com.esgi.unitest.todolist.components.todolist.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping("/user")
    public String user(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format("User %s!", name);
    }

}
