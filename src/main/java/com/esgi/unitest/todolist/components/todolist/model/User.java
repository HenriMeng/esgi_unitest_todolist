package com.esgi.unitest.todolist.components.todolist.model;

import com.esgi.unitest.todolist.exception.UserException;

import java.util.regex.Pattern;

public class User {

    String firstname;
    String lastname;
    int age;
    String email;
    String password;

    public User(String firstname, String lastname, int age, String email, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.email = email;
        this.password = password;
    }


    // METHODS --------------------------------------------------------------------------------------------


    public boolean is_valid() throws UserException {

        if (this.valid_password() &&
            this.valid_email_format() &&
            this.valid_age() &&
            this.valid_firstname() &&
            this.valid_lastname()) {
            return true;
        }

        throw new UserException("user is not valid");
    }


    public boolean valid_firstname() throws UserException {

        if (Pattern.matches("[a-zA-Z]+", this.firstname)) {
            return true;
        }

        throw new UserException("firstname is not valid (number are not allowed)");
    }


    public boolean valid_lastname() throws UserException {

        if (Pattern.matches("[a-zA-Z]+", this.lastname)) {
            return true;
        }

        throw new UserException("lastname is not valid (number are not allowed)");
    }


    public boolean valid_password() throws UserException {

        if (this.password.length() >= 8 && this.password.length() <= 40) {
            return true;
        }

        throw new UserException("Wrong password (min: 8, max: 40)");
    }


    public boolean valid_age() throws UserException {

        if (this.age >= 13) {
            return true;
        }

        throw new UserException("PEGI 13");
    }


    public boolean valid_email_format() throws UserException {

        if (Pattern.matches(".+@[a-zA-Z]+\\.[a-zA-Z]+", this.email) == true) {
            return true;
        }

        throw new UserException("email format not valid");
    }


    // GET&SET --------------------------------------------------------------------------------------------


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
