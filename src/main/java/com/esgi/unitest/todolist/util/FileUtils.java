package com.esgi.unitest.todolist.util;

/**
 * class with static methods
 * ONLY generic methods
 */
public class FileUtils {

    private FileUtils() {}

    public static String say_hello() {
        System.out.printf("Hello !");
        return "Hello !";
    }

}
